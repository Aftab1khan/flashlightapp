package com.example.aftab.flashlightapplication;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.CheckBoxPreference;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener{
ImageButton btnSwitch;

    Camera camera;
    Parameters parameters;
    private boolean isFlash;
    private boolean hasFlash;
    MediaPlayer mp;
    CheckBoxPreference checkBoxPreference;
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnSwitch = (ImageButton) findViewById(R.id.btnSwitch);
        hasFlash = getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
        if (!hasFlash) {
            AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
            alertDialog.setTitle("Error");
            alertDialog.setMessage("Sorryy ur app doesnt support");
            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            alertDialog.show();
            return;
        }

        getCamera();
        toggleButtonImage();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        //SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);


            btnSwitch.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {


                     if (isFlash ) {
                             turnOfFlash();

                         if( sharedPreferences.contains(getString(R.string.show_bass)) && sharedPreferences.getBoolean(getString(R.string.show_bass),false) == true) {
                             Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                             vibrator.vibrate(1000);
                         }
                    }

                    else if(!isFlash) {
                         turnOnFlash();
                         if(sharedPreferences.contains(getString(R.string.show_bass)) && sharedPreferences.getBoolean(getString(R.string.show_bass),false) == true){
                             Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                             vibrator.vibrate(1000);
                         }



                    }
                }

            });


            sharedPreferences.registerOnSharedPreferenceChangeListener(this);
        }


    public void getCamera()
    {
        if(camera==null){
            try{
                camera= Camera.open();
            parameters=camera.getParameters();
            }
            catch (RuntimeException e){
     Log.e("camera errer",e.getMessage());
            }
        }
    }
private void turnOfFlash(){
    if (isFlash){
 if (camera==null || parameters==null){
     return;
 }
  playSound();
        parameters=camera.getParameters();
        parameters.setFlashMode(Parameters.FLASH_MODE_OFF);
        camera.setParameters(parameters);
        camera.stopPreview();
        isFlash=false;
        toggleButtonImage();

    }
}
    private void turnOnFlash() {
        if (!isFlash) {
            if (camera == null || parameters == null) {
                return;
            }
            playSound();
            parameters = camera.getParameters();
            parameters.setFlashMode(Parameters.FLASH_MODE_TORCH)
            ;
            camera.setParameters(parameters);
            camera.startPreview();
            isFlash = true;
            toggleButtonImage();
        }

    }
    private void playSound(){
        if(isFlash){
            mp = MediaPlayer.create(MainActivity.this, R.raw.light_switch_off);
        }else{
            mp = MediaPlayer.create(MainActivity.this, R.raw.light_switch_on);
        }
        mp.setOnCompletionListener(new OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                // TODO Auto-generated method stub
                mp.release();
            }
        });
        mp.start();
    }
    private void toggleButtonImage(){
        if(isFlash){
            btnSwitch.setImageResource(R.drawable.btn_switch_on);
        }else{
            btnSwitch.setImageResource(R.drawable.btn_switch_off);
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // on pause turn off the flash

    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // on resume turn on the flash
        if(hasFlash)
            turnOnFlash();
    }

    @Override
    protected void onStart() {
        super.onStart();

        // on starting the app get the camera params
        getCamera();
    }

    @Override
    protected void onStop() {
        super.onStop();

        // on stop release the camera
        if (camera != null) {
            camera.release();
            camera = null;
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        if(id==R.id.search)
        {
            Intent intent=new Intent(this,SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
     if(key.equals(getString(R.string.show_bass))){
       sharedPreferences.getBoolean(getString(R.string.show_bass), true);

     }
    }
}

